# system modules

# internal modules

# external modules


def walk(d, b=tuple()):
    """
    Generator recursing into a nested dictionary

    Args:
        d (dict): the arbitraryly-nested dictionary to recurse into
        b (flat tuple): tuple of layers to prepend. You should not need to
            specify this as it is used to store the recursion state.

    Yields:
        layers, deepestelement: tuple of recursed layers and the deepest
            element
    """
    if hasattr(d, "items"):
        for k, v in d.items():
            for m in walk(v, b + (k,)):
                yield m
    elif isinstance(d, str):
        yield (b, d)
    else:
        try:
            for i in iter(d):
                for i in walk(i, b):
                    yield i
        except TypeError:
            yield (b, d)
