# system modules
import logging
from functools import partial
import itertools

# internal modules

# external modules

logger = logging.getLogger(__name__)


def zip_measurements(
    station_data, sensor_heights, station_sensors, sensor_formats
):
    """
    Generator yielding data parsed from a measurement data array

    Args:
        station_data (sequence of sequences of sequences): the measurements
            to parse. In the form:

            .. code-block:: python

                [
                    [ # height 1
                        [quantity1],                       # sensor 1
                        [quantity1, quantity2, quantity3], # sensor 2
                        [quantity1, quantity2],            # sensor 3
                        ...
                    ],
                    [ # height 2
                        None,  # no sensor 1 at height 2
                        [quantity1, quantity2, quantity3], # sensor 2
                        [quantity1, quantity2],            # sensor 3
                        ...
                    ],
                    ...
                ]

        station_heights (sequence of numeric): the heights of the sensors.
            Must have the same length as ``station_data``.
        station_sensors (sequence of str): the name of the sensors present
            at each height. Must have the same length as the sequences in the
            second level of ``station_data``, e.g. ``station_data[0][0]``
        sensor_formats (dict, optional): mapping of sensor names to tuples of
            quantity definitions. A quantity definition is itself a tuple like
            ``(quantity_name, unit, converter)``. ``unit`` (default: ``None``)
            and ``converter`` (default: no conversion) can be left out and a
            quantity definition can also be only the string ``quantity_name``.
            The converter a callable which will be called with the measurement
            value as single argument and which shall return the value in the
            specified ``unit``.

    Yields:
        dict : mapping with the following keys:

            ``"quantity"``

                the quantity name as specified in ``sensor_formats``

            ``"unit"``

                the unit as specified in ``sensor_formats``

            ``"sensor"``

                the sensor name as specified in ``station_sensors``

            ``"height"``

                the height as specified in ``station_heights``

            ``"value"``

                the measurement value from ``station_data`` converted with the
                converter specified in ``sensor_formats``
                ``sensor_formats``
    """
    for height_index, (height, sensors) in enumerate(
        itertools.zip_longest(sensor_heights, station_data)
    ):
        if height is None:
            logger.warning(
                "No more heights in format "
                "for station with id {id}"
                "to associate data {data} at index {index} to. "
                "Continuing with unknown height.".format(
                    id=info.get("id"), data=sensors, index=height_index
                )
            )
        if sensors is None:
            logger.debug(
                "No sensor data available for height "
                "{height}. Skipping.".format(height=height)
            )
            continue
        for sensor_index, (sensor, measurements) in enumerate(
            itertools.zip_longest(
                station_sensors, sensors, fillvalue=Exception
            )
        ):
            if sensor is Exception:
                logger.error(
                    "Don't know what to do with measurements "
                    "{data} at index {index}: "
                    "no sensor format available".format(
                        index=sensor_index, data=measurements
                    )
                )
                continue
            if measurements is Exception:
                logger.error(
                    "measurements {data} are missing "
                    "data for sensor {sensor}".format(
                        data=sensors, index=sensor_index, sensor=sensor
                    )
                )
                continue
            sensor_quantities_formats = sensor_formats.get(sensor)
            if not sensor_quantities_formats:
                logger.error(
                    "Unknown sensor format '{sensor}' at index "
                    "{index}. Don't know what to do "
                    "with measurements {data}".format(
                        sensor=repr(sensor),
                        data=measurements,
                        index=sensor_index,
                    )
                )
                continue
            if not measurements:
                measurements = tuple(None for f in sensor_quantities_formats)
            for measurement_index, (fmt, measurement) in enumerate(
                itertools.zip_longest(
                    sensor_quantities_formats,
                    measurements,
                    fillvalue=Exception,
                )
            ):
                if fmt is Exception:
                    logger.error(
                        "Don't know what to do "
                        "with measurement {data} at "
                        "index {index} of sensor {sensor}: "
                        "no quantity format".format(
                            data=repr(measurement),
                            index=measurement_index,
                            sensor=repr(sensor),
                        )
                    )
                    continue
                fmt_iter = iter((fmt,) if isinstance(fmt, str) else fmt)
                quantity, unit, converter = map(
                    partial(next, fmt_iter), ("?", None, lambda x: x)
                )
                if measurement is Exception:
                    logger.warning(
                        "{sensor} measurements {data}"
                        "is missing {quantity} data".format(
                            sensor=repr(sensor),
                            quantity=repr(quantity),
                            data=measurements,
                        )
                    )
                    continue
                if measurement is None:
                    converted = measurement
                else:
                    try:
                        converted = converter(measurement)
                    except BaseException as e:
                        logger.error(
                            "Could not convert "
                            "{sensor} measurement {data} "
                            "with {converter}: "
                            "{etype}: {error}".format(
                                sensor=repr(sensor),
                                data=measurement,
                                converter=converter.__name__,
                                etype=type(e).__name__,
                                error=e,
                            )
                        )
                        continue
                yield {
                    "height": height,
                    "sensor": sensor,
                    "quantity": quantity,
                    "unit": unit,
                    "value": converted,
                }
