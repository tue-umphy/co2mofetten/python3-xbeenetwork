# system modules

# internal modules
from central.cli.commands.main import cli

# external modules

if __name__ == "__main__":
    cli(prog_name="python3 -m central")
