# system modules
import logging
import time
from functools import partial

# internal modules

# external modules
import attr

logger = logging.getLogger(__name__)

__doc__ = """
Very loose Json-RPC-2.0-like implementation
"""


@attr.s
class Message:
    @staticmethod
    def from_dict(d):
        def dict_to_object(d):
            for cls in (Error, Request, Response, Notification):
                try:
                    yield cls.from_dict(d)
                except (ValueError, TypeError, AttributeError):
                    continue

        return next(dict_to_object(d), None)


@attr.s
class BaseRequest:
    method = attr.ib(validator=attr.validators.instance_of(str))
    params = attr.ib(default=None)

    @params.validator
    def _params_validator(self, attribute, value):
        if value is not None and type(value) not in (dict, tuple, list):
            raise ValueError(
                "{}.params must be dict, tuple or list".format(
                    type(self).__name__
                )
            )

    def to_dict(self):
        d = {"method": self.method}
        if self.params is not None:
            d["params"] = self.params
        return d

    @classmethod
    def from_dict(cls, d):
        return cls(method=d.get("method"), params=d.get("params"))


@attr.s
class Notification(BaseRequest):
    pass


@attr.s
class Request(BaseRequest):
    id = attr.ib(default=None)

    @id.validator
    def _id_validator(self, attribute, value):
        if type(value) not in (int, str):
            raise ValueError(
                "{cls}.id must be int or str, "
                "not {given} which is {givencls}".format(
                    cls=type(self).__name__,
                    given=value,
                    givencls=type(value).__name__,
                )
            )

    def to_dict(self):
        d = BaseRequest.to_dict(self)
        d["id"] = self.id
        return d

    @classmethod
    def from_dict(cls, d):
        return cls(
            method=d.get("method"), params=d.get("params"), id=d.get("id")
        )


@attr.s
class BaseResponse:
    pass


@attr.s
class Response(BaseResponse):
    result = attr.ib()

    @result.validator
    def _result_validator(self, attribute, value):
        if value is None:
            raise ValueError("{} must not be None".format(type(self).__name__))

    id = attr.ib(validator=attr.validators.instance_of(int))

    def to_dict(self):
        return {"result": self.result, "id": self.id}

    @classmethod
    def from_dict(cls, d):
        return cls(result=d.get("result"), id=d.get("id"))


@attr.s
class ErrorObject:
    code = attr.ib(validator=attr.validators.instance_of(int))
    message = attr.ib(validator=attr.validators.instance_of(str))
    data = attr.ib(default=None)

    def to_dict(self):
        d = {"code": self.code, "message": self.message}
        if self.data is not None:
            d["data"] = self.data
        return d

    @classmethod
    def from_dict(cls, d):
        return cls(
            code=d.get("code"), message=d.get("message"), data=d.get("data")
        )


@attr.s
class Error(BaseResponse):
    error = attr.ib(validator=attr.validators.instance_of(ErrorObject))
    id = attr.ib(
        default=None,
        validator=attr.validators.optional(attr.validators.instance_of(int)),
    )

    def to_dict(self):
        return {"id": self.id, "error": self.error.to_dict()}

    @classmethod
    def from_dict(cls, d):
        return cls(
            error=ErrorObject.from_dict(d.get("error", {})), id=d.get("id")
        )


class DispatchError(Exception):
    pass


class NoSuchMethod(DispatchError):
    pass


class NoRequest(DispatchError):
    pass


@attr.s
class Dispatcher:
    methods = attr.ib(default=attr.Factory(dict), converter=dict)

    def register_method(self, name, method):
        self.methods[name] = method

    def method(self, method):
        """
        Decorator for adding a method
        """
        self.register_method(method.__name__, method)
        return method

    def handle(self, x, *args, **kwargs):
        if isinstance(x, dict):
            return self.handle_dict(x, *args, **kwargs)
        elif isinstance(x, BaseRequest):
            return self.handle_request(x, *args, **kwargs)
        else:
            raise NoRequest(
                "Given message {} is neither "
                "dict nor Request but {}".format(x, type(x).__name__)
            )

    def handle_dict(self, d, *args, **kwargs):
        message = Message.from_dict(d)
        if any(map(partial(isinstance, message), (Request, Notification))):
            return self.handle_request(message, *args, **kwargs)
        else:
            raise NoRequest(
                "Given message {} is not a Request but {}".format(
                    d, type(message).__name__
                )
            )

    def handle_request(self, request, *args, **kwargs):
        if request.method in self.methods:
            return self.methods.get(request.method)(request, *args, **kwargs)
        else:
            raise NoSuchMethod(request.method)


class ClientError(Exception):
    pass


class NoSuchRequest(ClientError):
    pass


@attr.s
class Client:
    """
    RPC client
    """

    requests = attr.ib(default=attr.Factory(dict), converter=dict)
    id = attr.ib(default=0, converter=lambda n: (n if n < 256 else 0))

    def request(self, *args, callback, callback_kwargs={}, **kwargs):
        kwargs.update(id=self.id)
        request = Request(*args, **kwargs)
        self.requests[self.id] = partial(callback, **callback_kwargs)
        self.id += 1
        return request

    def process(self, response):
        callback = self.requests.get(response.id)
        if callback:
            # logger.debug("Calling callback")
            ret = callback(response)
            # logger.debug("Removing ID")
            self.requests.pop(response.id, None)
            return ret
        else:
            raise NoSuchRequest(
                "No callback registered for id {}".format(response.id)
            )

    def wait_for_response(self, request, timeout=float("inf")):
        time_before = time.time()
        while True:
            if request.id not in self.requests:
                return True
            if time.time() - time_before > timeout:
                return False
            time.sleep(0.1)
