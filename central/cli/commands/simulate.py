# system modules
import argparse
import logging
import time
import random
from urllib.parse import urlparse
from functools import partial

# internal modules
from central.cli.commands.main import cli

# external modules
import paho.mqtt.client as mqtt
import click

logger = logging.getLogger(__name__)


def generate_choices(n_boxes=10, heights=(50, 100, 150, 200)):
    return {
        "central": {
            "maximet": {
                ("temperature", "°C"): lambda: round(
                    random.uniform(-10, 50), 2
                ),
                ("humidity", "%"): lambda: random.randint(0, 100),
                ("pressure", "hPa"): lambda: round(
                    random.uniform(950, 1050), 2
                ),
                ("wind-speed", "m/s"): lambda: round(random.uniform(0, 10), 1),
                ("wind-dir", "m/s"): lambda: random.randint(0, 359),
                ("solar-rad", "W/m^2"): lambda: round(
                    random.uniform(0, 1200), 2
                ),
            },
            "rpi": {
                "cpu": {
                    ("temperature", "°C"): lambda: round(
                        random.uniform(20, 70), 2
                    )
                }
            },
        },
        "irgason": {
            ("sonic-temperature", "°C"): lambda: round(
                random.uniform(-10, 50), 2
            ),
            ("wind", "m/s"): lambda: round(random.uniform(0, 10), 1),
            ("co2", "ppm"): lambda: random.randint(0, 3000),
            ("h2o", "ppm"): lambda: random.randint(0, 40000),
        },
        "box": {
            lambda: random.randint(1, n_boxes): {
                "height": {
                    lambda: random.choice(heights): {
                        "scd30": {
                            ("temperature", "°C"): lambda: round(
                                random.uniform(-10, 50), 2
                            ),
                            ("humidity", "%"): lambda: random.randint(0, 100),
                            ("co2", "ppm"): lambda: random.randint(0, 40000),
                        },
                        "bme280": {
                            ("temperature", "°C"): lambda: round(
                                random.uniform(-10, 50), 2
                            ),
                            ("humidity", "%"): lambda: random.randint(0, 100),
                            ("pressure", "hPa"): lambda: round(
                                random.uniform(950, 1050), 2
                            ),
                        },
                    }
                },
                "sd": {
                    ("size-total", "bytes"): lambda: 2
                    ** random.randint(20, 25),
                    ("size-used", "bytes"): lambda: random.randint(
                        0, 2 ** random.randint(20, 25)
                    ),
                    "status": lambda: random.choice(("ok", "fail")),
                },
                "gps": {
                    "status": lambda: random.choice(("no-fix", "fix", "fail")),
                    ("lat", "°"): lambda: round(random.uniform(42.5, 42.6), 6),
                    ("lon", "°"): lambda: round(random.uniform(9.5, 9.6), 6),
                },
            }
        },
    }


def draw(d):
    # draw a random key from the dict
    key = random.choice(tuple(d))
    # extract the corresponding value
    v = d[key]
    # interpret the key as one or more elements
    try:
        it = iter((key,) if isinstance(key, str) else key)
    except TypeError:
        it = iter((key,))
    # split the key into quantity and unit
    quantity, unit = tuple(next(it, None) for i in range(2))
    # if the quantity is callable, call it
    first = quantity() if hasattr(quantity, "__call__") else quantity
    # if the value is callable, call it, otherwise interpret it as another dict
    # to draw from
    second = v() if hasattr(v, "__call__") else draw(v)
    # if there was a unit, append it to the value
    if unit:
        second = " ".join(map(str, (second, unit)))
    # concatenate elements into a flat tuple
    return (first,) + ((second,) if isinstance(second, str) else second)


def url(arg):
    return urlparse(
        "mqtt://{}".format(arg) if not arg.startswith("mqtt://") else arg,
        scheme="mqtt",
    )


@cli.command(name="mqtt_simulate", help="simulate MQTT publishments")
@click.option(
    "-b",
    "--broker",
    "--host",
    "mqtt_brokers",
    help="MQTT brokers to connect to. "
    "Can be specified multiple times. "
    "Supports user:pass@host:port format",
    type=url,
    default=("localhost:1883",),
    show_envvar=True,
    multiple=True,
)
@click.option(
    "-n",
    "--name",
    "mqtt_client_name",
    help="MQTT client name",
    default="central",
)
@click.option("-p", "--prefix", "prefix_topic", help="prefix topic to prepend")
@click.option(
    "--n-boxes",
    help="Number of different boxes",
    type=click.IntRange(min=1),
    default=10,
)
@click.option(
    "--heights",
    help="What heights to be available",
    type=lambda x: tuple(map(int, x.split(",") if hasattr(x, "split") else x)),
    default=(50, 100, 150, 200),
    metavar="HEIGHT1,HEIGHT2,...",
)
@click.option(
    "--delay",
    "-d",
    "delay_range",
    nargs=2,
    type=lambda x: abs(float(x)),
    metavar="MIN MAX",
    help="Minimum and maximum for random seconds delay between publishments",
    default=(1, 5),
)
def simulate(
    mqtt_brokers, mqtt_client_name, prefix_topic, n_boxes, heights, delay_range
):
    def on_connect(client, userdata, flags, rc):
        logger.info(
            "Client {client._client_id} {status} connected to "
            "{client._host}:{client._port} (code {rc})".format(
                client=client, rc=rc, status="now" if rc == 0 else "not"
            )
        )

    def on_disconnect(client, userdata, flags):
        logger.info(
            "Client {client._client_id} now disconnected from "
            "{client._host}:{client._port}".format(client=client)
        )

    def create_client(url, name=mqtt_client_name):
        client = mqtt.Client(name)
        client.connect_async(
            url.hostname, url.port if url.port is not None else 1883
        )
        client.username_pw_set(url.username, url.password)
        client.on_connect = on_connect
        logger.info(
            "Client {client._client_id} connecting to "
            "{client._host}:{client._port}...".format(client=client)
        )
        client.loop_start()
        return client

    clients = tuple(map(create_client, mqtt_brokers))

    choices = generate_choices(n_boxes=n_boxes, heights=heights)

    try:

        while True:
            drawn = ((prefix_topic,) if prefix_topic else tuple()) + draw(
                choices
            )
            topic, message = "/".join(map(str, drawn[:-1])), drawn[-1]
            for client in clients:
                logger.debug(
                    "Trying to publish {message} to topic {topic} "
                    "on {client._host}:{client._port}".format(
                        topic=repr(topic), message=repr(message), client=client
                    )
                )
                messageinfo = client.publish(topic, message)
                if messageinfo.rc == mqtt.MQTT_ERR_SUCCESS:
                    logger.info(
                        "Successfully published {message} to topic "
                        "{topic} on {client._host}:{client._port}".format(
                            topic=repr(topic),
                            message=repr(message),
                            client=client,
                        )
                    )
                elif messageinfo.rc == mqtt.MQTT_ERR_NO_CONN:
                    logger.error(
                        "no connection to "
                        "{client._host}:{client._port}".format(client=client)
                    )
                else:
                    logger.debug(
                        "Client on {client._host}:{client._port}: "
                        "messageinfo.rc = {rc}".format(
                            rc=messageinfo.rc, client=client
                        )
                    )
            delay = random.uniform(min(delay_range), max(delay_range))
            logger.debug("Waiting {} seconds".format(delay))
            time.sleep(delay)
    except KeyboardInterrupt:
        pass
