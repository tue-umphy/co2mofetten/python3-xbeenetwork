Welcome to central's documentation!
===================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   changelog
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
