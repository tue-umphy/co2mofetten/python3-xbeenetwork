# system modules
import logging
import configparser
import random
import operator
import itertools
from functools import partial
import re
from urllib.parse import urlparse

# internal modules

# external modules
import asteval
import paho.mqtt.client as mqtt

logger = logging.getLogger(__name__)


class ConfigurationError(ValueError):
    pass


class BrokerSectionError(ConfigurationError):
    pass


class SensorFormatSectionError(ConfigurationError):
    pass


class Configuration(configparser.ConfigParser):
    """
    Class for configuration
    """

    BROKER_SECTION_PREFIX = "broker"
    """
    Prefix for sections specifying an MQTT broker
    """

    SENSOR_FORMAT_PREFIX = "sensor-format"
    """
    Prefix for sections specifying a sensor format
    """

    DEFAULT_CLIENT_STATION_NAME = "central"

    DEFAULT_CLIENT_DATA_TOPIC_TEMPLATE = (
        "{station}/{height}/{sensor}/{quantity}"
    )
    DEFAULT_CLIENT_STATUS_TOPIC_TEMPLATE = "{station}/{component}/status"
    DEFAULT_CLIENT_STATUS_SENSORS_TOPIC_TEMPLATE = (
        "{station}/{height}/{component}/works"
    )
    DEFAULT_CLIENT_DATA_MESSAGE_TEMPLATE = "{value} {unit}"

    def __init__(self, **kwargs):
        configparser.ConfigParser.__init__(
            self,
            **{**{"allow_no_value": True, "interpolation": None}, **kwargs}
        )

    @property
    def broker_sections(self):
        """
        Generator yielding broker sections

        Yields:
            configparser.SectionProxy: the next broker section
        """
        for name, section in self.items():
            if name.startswith(self.BROKER_SECTION_PREFIX):
                yield section

    @property
    def sensor_format_sections(self):
        """
        Generator yielding sensor format sections

        Yields:
            configparser.SectionProxy: the next sensor format section
        """
        for name, section in self.items():
            if name.startswith(self.SENSOR_FORMAT_PREFIX):
                yield section

    @staticmethod
    def parse_quantity_spec(quantity, spec):
        spec = spec if spec else ""
        match = re.fullmatch(
            r"\s*(?:(?P<converterstr>.*?)?\s*[=-]+>+\s*)?"
            r"(?:\[(?P<unit>[^]]+)\])?\s*",
            spec,
        )
        if not match:
            raise SensorFormatSectionError(
                "invalid quantity specification {spec} for {quantity}".format(
                    quantity=repr(quantity), spec=repr(spec)
                )
            )
        gd = match.groupdict()
        converterstr = gd.get("converterstr")

        def create_converter(s):
            interpreter = asteval.Interpreter()

            def converter(x):
                interpreter.symtable["x"] = x
                return interpreter(s)

            return converter

        converter = (
            create_converter(converterstr) if converterstr else lambda x: x
        )
        unit = gd.get("unit")
        if not unit:
            unit = ""
        return (quantity, unit, converter)

    @property
    def sensor_formats(self):
        """
        Construct the sensor formats from the current configuration
        """
        formats = {}
        for section in self.sensor_format_sections:
            match = re.fullmatch(
                r"{prefix}\W+(?P<sensor>.*)".format(
                    prefix=self.SENSOR_FORMAT_PREFIX
                ),
                section.name,
            )
            if not match:
                raise SensorFormatSectionError(
                    "Sensor format section name {section.name} "
                    "does not contain a sensor name".format(section=section)
                )
            sensor = match.groupdict()["sensor"]
            formats[sensor] = tuple(
                self.parse_quantity_spec(q, s) for q, s in section.items()
            )
        return formats

    @property
    def clients(self):
        """
        Generator yielding set-up clients from the sections

        Yields:
            paho.mqtt.client.Client: the set-up but unstarted clients
        """
        for section in self.broker_sections:
            client_name = section.get(
                "client-name",
                "central-{}".format(random.randint(1, 2 ** 16 - 1)),
            )
            client = mqtt.Client(client_name)
            client.username_pw_set(
                section.get("username"), section.get("password")
            )
            client._data_topic_template = section.get(
                "data_topic", self.DEFAULT_CLIENT_DATA_TOPIC_TEMPLATE
            )
            client._data_message_template = section.get(
                "data_message", self.DEFAULT_CLIENT_DATA_MESSAGE_TEMPLATE
            )
            client._status_topic_template = section.get(
                "status_topic", self.DEFAULT_CLIENT_STATUS_TOPIC_TEMPLATE
            )
            client._status_sensors_topic_template = section.get(
                "status_topic_sensors",
                self.DEFAULT_CLIENT_STATUS_SENSORS_TOPIC_TEMPLATE,
            )
            client._station_name = section.get(
                "station_name", self.DEFAULT_CLIENT_STATION_NAME
            )
            client.connect_async(
                section.get("hostname", "localhost"),
                section.getint("port", 1883),
            )
            yield client

    def add_section(self, name):
        """
        Add a new section with :any:`configparser.ConfigParser.add_section` but
        but if the name already exists, append something so that it doesn't
        exist.

        Args:
            name (str): the name of the new section

        Returns:
            Section : the newly created section
        """
        nonexistant_name = next(
            filter(
                lambda x: x not in self,
                itertools.chain(
                    (name,),
                    (
                        " ".join(map(str, (name, "nr.", nr)))
                        for nr in itertools.count(2)
                    ),
                ),
            )
        )
        configparser.ConfigParser.add_section(self, nonexistant_name)
        return self[nonexistant_name]
