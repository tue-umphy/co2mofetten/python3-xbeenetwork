# system modules
import logging
import functools
import warnings
from collections import defaultdict
import itertools
import time

# internal modules
from central.rpc import (
    Dispatcher,
    Message,
    Notification,
    Request,
    Response,
    Client,
    NoRequest,
    NoSuchMethod,
    ClientError,
)
from central.scheduler import Scheduler

# external modules
import attr
from digi.xbee.devices import XBeeDevice
from digi.xbee.exception import XBeeException
import msgpack


logger = logging.getLogger(__name__)


@attr.s
class XBeeMsgPackRPC:
    """
    Class for XBee central device
    """

    device = attr.ib(validator=attr.validators.instance_of(XBeeDevice))
    dispatcher = attr.ib(
        default=attr.Factory(Dispatcher),
        validator=attr.validators.instance_of(Dispatcher),
    )
    client = attr.ib(
        default=attr.Factory(Client),
        validator=attr.validators.instance_of(Client),
    )
    scheduler = attr.ib(
        default=attr.Factory(Scheduler),
        validator=attr.validators.instance_of(Scheduler),
    )
    _last_comm = attr.ib(default=attr.Factory(lambda: defaultdict(float)))
    min_comm_interval = attr.ib(default=1, converter=float)
    _extra_pre_data_recieved_callbacks = attr.ib(
        default=attr.Factory(list), converter=list
    )
    _extra_post_data_recieved_callbacks = attr.ib(
        default=attr.Factory(list), converter=list
    )

    def data_recieved_callback_pre(self, cb):
        """
        Decorator to add extra data recieved callbacks to execute before
        processing RPC
        """
        self._extra_pre_data_recieved_callbacks.append(cb)
        return cb

    def data_recieved_callback_post(self, cb):
        """
        Decorator to add extra data recieved callbacks to execute before
        processing RPC
        """
        self._extra_post_data_recieved_callbacks.append(cb)
        return cb

    def only_if_device_open(self, decorated_function):
        """
        Decorator for functions to be skipped when the :any:`device` is not
        open
        """

        @functools.wraps(decorated_function)
        def wrapper(*args, **kwargs):
            if self.device.is_open():
                return decorated_function(*args, **kwargs)
            else:
                logger.debug(
                    "Skip calling {fun.__name__} because "
                    "device {device._serial_port.port} is not open".format(
                        fun=decorated_function, device=self.device
                    )
                )

        return wrapper

    def rpc_callback(self, xbee_message):
        try:
            decoded = msgpack.unpackb(xbee_message.data, raw=False)
        except msgpack.UnpackException as e:
            logger.warning(
                "{raw} is not MsgPack: {error}".format(
                    raw=xbee_message.data, error=e
                )
            )
            return
        logger.debug(
            "Recieved {msgtype} message from {addr} "
            ": {raw} : {msgpack}".format(
                msgtype="broadcast"
                if xbee_message.is_broadcast
                else "unicast",
                addr=xbee_message.remote_device.get_64bit_addr(),
                raw=xbee_message.data,
                msgpack=decoded,
            )
        )
        message = Message.from_dict(decoded)
        try:
            try:
                response = self.dispatcher.handle(message, xbee_message)
                if isinstance(message, Notification):
                    if isinstance(response, Message):
                        warnings.warn(
                            "Refusing to respond {response} returned from "
                            "registered callback for notification "
                            "{message}!".format(
                                message=xbee_message, response=response
                            )
                        )
                    return
                if not isinstance(response, Response):
                    warnings.warn(
                        "The registered callback method for "
                        "{message} didn't return a valid response "
                        "(just {response})!"
                        "Using default response.".format(
                            message=xbee_message, response=response
                        )
                    )
                    response = Response(
                        id=getattr(message, "id", 0), result="?"
                    )
                encoded = msgpack.packb(response.to_dict())
                logger.debug(
                    "Responding {} ==> {} ==> {} to XBee device {}".format(
                        response,
                        response.to_dict(),
                        encoded,
                        xbee_message.remote_device.get_64bit_addr(),
                    )
                )
                try:
                    self.device.send_data(xbee_message.remote_device, encoded)
                except XBeeException as e:
                    logger.error(
                        "{etype} transmitting to XBee {addr}: {error}".format(
                            addr=xbee_message.remote_device.get_64bit_addr(),
                            error=e,
                            etype=type(e).__name__,
                        )
                    )
            except NoSuchMethod as e:
                logger.error(
                    "{etype} during message processing: {e}".format(
                        etype=type(e).__name__, e=e
                    )
                )
        except NoRequest:
            try:
                self.client.process(message)
            except ClientError as e:
                logger.error(
                    "{etype} during message processing: {e}".format(
                        etype=type(e).__name__, e=e
                    )
                )

    def encode(self, message):
        return msgpack.packb(message.to_dict())

    def decode(self, encoded):
        return msgpack.unpackb(encoded, raw=False)

    def send(self, message, remote_xbee):
        addr = str(remote_xbee.get_64bit_addr())
        seconds = self.min_comm_interval - (
            time.time() - self._last_comm[addr]
        )
        if seconds > 0:
            logger.debug(
                "Waiting another {diff:.2f} seconds "
                "before sending to {addr}...".format(diff=seconds, addr=addr)
            )
            time.sleep(seconds)
        logger.debug(
            "Sending  "
            "{message} "
            "==> {dict} "
            "==> {msgpack}"
            "to remote XBee {addr}".format(
                message=message,
                dict=message.to_dict(),
                msgpack=self.encode(message),
                addr=addr,
            )
        )
        try:
            self.device.send_data(remote_xbee, self.encode(message))
        except XBeeException as e:
            logger.error(
                "{etype} sending {message} "
                "==> {dict} "
                "==> {msgpack} "
                "to remote XBee {addr}"
                ": {error}".format(
                    error=e,
                    etype=type(e).__name__,
                    dict=message.to_dict(),
                    message=message,
                    msgpack=self.encode(message),
                    addr=addr,
                )
            )
        self._last_comm[addr] = time.time()

    def broadcast(self, message):
        seconds = self.min_comm_interval - (
            time.time()
            - (max(self._last_comm.values()) if self._last_comm else 0)
        )
        if seconds > 0:
            logger.debug(
                "Waiting another {diff:.2f} seconds "
                "before broadcasting...".format(diff=seconds)
            )
            time.sleep(seconds)
        asdict = message.to_dict()
        encoded = msgpack.packb(asdict)
        logger.debug(
            "Broadcasting  "
            "{message} "
            "==> {dict} "
            "==> {msgpack}".format(
                message=message, dict=message.to_dict(), msgpack=encoded
            )
        )
        try:
            self.device.send_data_broadcast(encoded)
        except XBeeException as e:
            logger.error(
                "{etype} broadcasting {message} "
                "==> {dict} "
                "==> {msgpack} "
                ": {error}".format(
                    error=e,
                    etype=type(e).__name__,
                    dict=encoded,
                    message=message,
                    msgpack=encoded,
                )
            )
        for k in self._last_comm:
            self._last_comm[k] = time.time()

    def run(self):
        logger.debug("Opening XBee device")
        self.device.open()
        for cb in self._extra_pre_data_recieved_callbacks:
            self.device.add_data_received_callback(cb)
        self.device.add_data_received_callback(self.rpc_callback)
        for cb in self._extra_post_data_recieved_callbacks:
            self.device.add_data_received_callback(cb)
        logger.debug("Starting scheduler")
        self.scheduler.run()

    def stop(self):
        self.scheduler.stop()
        if self.device.is_open():
            self.device.close()
