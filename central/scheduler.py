# system modules
import logging
import threading
import time
from abc import ABC, abstractproperty, abstractmethod
from collections import defaultdict

# internal modules

# external modules
import attr

logger = logging.getLogger(__name__)


@attr.s
class Repeater(ABC):
    """
    Base class for Repeaters
    """

    @abstractproperty
    def due(self):
        pass

    @abstractmethod
    def advance(self):
        pass


@attr.s
class IntervalRepeater(Repeater):
    """
    Repeater for fixed intervals
    """

    interval = attr.ib(converter=float)
    last_time = attr.ib(default=0, converter=float)

    @property
    def due(self):
        return time.time() - self.last_time > self.interval

    def advance(self):
        self.last_time = time.time()


@attr.s
class ArbitraryRepeater(Repeater):
    """
    Repeater for arbitrary intervals
    """

    determinator = attr.ib(validator=lambda o, a, v: hasattr(v, "__call__"))

    @property
    def due(self):
        return bool(self.determinator())

    def advance(self):
        pass


@attr.s
class Event:
    """
    Class for repeated events
    """

    repeater = attr.ib(validator=attr.validators.instance_of(Repeater))
    function = attr.ib(validator=lambda o, a, v: hasattr(v, "__call__"))

    def process(self):
        if self.repeater.due:
            self.function()
            self.repeater.advance()


@attr.s
class Scheduler:
    """
    Class to schedule things
    """

    events = attr.ib(default=attr.Factory(list), converter=list)
    delay = attr.ib(default=0.1, converter=float)
    _stopevent = attr.ib(
        default=attr.Factory(threading.Event),
        validator=attr.validators.instance_of(threading.Event),
    )

    def schedule(self, repeater):
        def decorator(decorated_function):
            event = Event(function=decorated_function, repeater=repeater)
            self.events.append(event)
            return decorated_function

        return decorator

    def stop(self):
        logger.debug("Will soon stop scheduling events")
        self._stopevent.set()

    def process(self, *args, **kwargs):
        while True:
            if self._stopevent.is_set():
                logger.info("Not continuing with scheduling")
                break
            for event in self.events:
                event.process()
            time.sleep(self.delay)

    def run(self, blocking=True):
        thread = threading.Thread(target=self.process, daemon=True)
        logger.debug("Starting event processing thread")
        thread.start()
        if blocking:
            logger.debug("Waiting for the event processing thread to exit")
            thread.join()
        else:
            return thread
