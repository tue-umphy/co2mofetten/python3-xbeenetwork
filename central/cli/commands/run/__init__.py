# system modules
import sys
import operator
import os
import re
from urllib.parse import urlparse, parse_qsl
from collections import defaultdict
from functools import partial
import itertools
import json
import random
import argparse
import logging
import io
import time

# internal modules
from central.version import __version__
from central import utils
from central.rpc import (
    Dispatcher,
    Message,
    Notification,
    Request,
    Response,
    Client,
)
from central.xbee import XBeeMsgPackRPC
from central.scheduler import IntervalRepeater
from central.cli.commands.main import cli
from central.cli.commands.run.parse import zip_measurements
from central.cli.commands.run.config import (
    Configuration,
    BrokerSectionError,
    SensorFormatSectionError,
)

# external modules
from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice
from digi.xbee.models.address import XBee64BitAddress
from digi.xbee.models.status import NetworkDiscoveryStatus
from digi.xbee.exception import XBeeException
from digi.xbee.io import IOLine, IOMode
from serial import SerialException
from serial.tools import list_ports
import msgpack
import click

logger = logging.getLogger(__name__)


def find_xbee_port():
    """
    lists ``ttyACM*`` and ``ttyUSB*`` ports and puts those with vendor id
    ``0x0403`` and product id ``0x6015`` first.
    """
    port = next(
        iter(
            sorted(
                list_ports.grep(r"tty(usb|acm)"),
                key=lambda x: ((x.vid if x.vid else 0) == 0x0403)
                + ((x.pid if x.pid else 0) == 0x6015),
                reverse=True,
            )
        ),
        None,
    )
    if port:
        return port.device
    else:
        raise click.UsageError(
            "Cannot determine XBee port automatically. "
            "Please specify a port via -p/--port."
        )


def scheme_url(scheme):
    def url(arg):
        return urlparse(
            arg
            if arg.startswith("{}://".format(scheme))
            else "{}://{}".format(scheme, arg),
            scheme=scheme,
        )

    return url


@cli.command()
@click.option(
    "-c",
    "--config",
    "configfiles",
    help="Add a configuration file to read. Can be specified multiple times.",
    type=click.Path(readable=True, exists=True, dir_okay=False),
    multiple=True,
    show_envvar=True,
    default=tuple(),
)
@click.option(
    "-p",
    "--port",
    help="XBee port",
    type=click.Path(exists=True, readable=True, dir_okay=False),
    default=find_xbee_port,
    show_default="try to find port automatically",
)
@click.option(
    "--broker",
    "mqtt_brokers",
    help="add another MQTT broker to publish the data to. "
    "Supports user:pass@host:port/TOPIC_TEMPLATE?OPTIONS format, "
    "where TOPIC_TEMPLATE is a topic template like /topic/{key1}/{key2} where "
    "the following keys enclosed in curly-brackets will be substituted: "
    "sensor,quantity,station,height,unit,value. "
    "Incoming data will be published to this topic. "
    "OPTIONS can be used for further customization. "
    "with the `message=...` option, a format for the MQTT message can be set. "
    "By default, the {value} is used as message. The same keys as in the "
    "topic are expanded. "
    "The `client_id=...` option can be used to set an id for the MQTT "
    "client. "
    "This option can be specified multiple times.",
    type=scheme_url("mqtt"),
    show_envvar=True,
    default=tuple(),
    multiple=True,
)
@click.option(
    "--baud", "baudrate", help="XBee port baudrate", default=9600, type=int
)
@click.pass_context
def run(ctx, port, baudrate, configfiles, mqtt_brokers):
    """
    Gateway between the XBee network and one or more MQTT brokers

    This subcommand connects both to an XBee coordinator device via a serial
    port and to one or more MQTT brokers and acts like a gateway, forwarding
    data requested from stations in the network to the brokers.

    The following tasks are performed regularly

    \b
    - broadcasting the time to all devices on the network
    - broadcasting the atmospheric pressure to all devices on the network
    - discovering devices on the network
    - requesting measurements from all devices on the network, by
        - requesting format information if not yet available
        - requesting data
        - publishing the data via MQTT

    # MQTT brokers

    A configuration file section for brokers might look like this with all
    fields being optional and the broker defaulting to a basic broker on
    localhost:

    \b
    [broker:mybroker]
    hostname = localhost
    port = 1883
    username = ...
    password = ...
    client_id = ...

    \b
    # here, keys enclosed in curly parentheses are expanded
    # topic template with
    topic = {station}/{height}/{sensor}/{quantity}
    # message template
    message = {value} {unit}

    # Sensor Formats

    As the XBee network restricts the sendable payload drastically (only up to
    72 bytes can be sent at once), the data format is optimized and only
    contains numeric values in a specific order. A mapping between these
    numeric values and actual meanings is negotiated via a format query between
    this XBee coordinator software and the remote stations. To conserve even
    more payload size, these format queries use predefined sensor format names
    that have to be defined for this software to be able to interpret the
    incoming data.

    A sensor format section could look like this:

    \b
    # the order of the quantities must match the order of the incoming data
    [sensor-format:SENSOR_FORMAT_NAME]
    # For optimization, QUANTITY1 is sent as an integer and needs to be divided
    # by 10 to yield a value in the desired unit. In the formula, 'x' is set to
    # the incoming value
    QUANTITY1 = x / 10 --> [UNIT1]
    # QUANTITY2 has no unit and does not need to be converted
    QUANTITY2
    # QUANTITY3 does not need to be converted but has a unit
    QUANTITY3 = [UNIT3]
    """
    config = Configuration()
    logger.debug("Reading configuration files {}".format(configfiles))
    config.read(configfiles)

    for broker in mqtt_brokers:
        logger.debug("Adding {} to configuration".format(broker))
        section = config.add_section(
            "{prefix}:{hostname}".format(
                prefix=config.BROKER_SECTION_PREFIX, hostname=broker.hostname
            )
        )
        section["hostname"] = (
            broker.hostname if broker.hostname else "localhost"
        )
        section["topic"] = (
            re.sub(r"^/", r"", broker.path)
            if broker.path
            else config.DEFAULT_CLIENT_DATA_TOPIC_TEMPLATE
        )
        if broker.query:
            mapping = dict(parse_qsl(broker.query, keep_blank_values=True))
            message_template = mapping.get("message")
            if message_template:
                section["message"] = message_template
            client_id = mapping.get("client_id")
            if client_id:
                section["client-id"] = client_id
        for attrib in ("port", "username", "password"):
            value = getattr(broker, attrib, None)
            if value is not None:
                section[attrib] = str(value)

    for section in (
        "xbee:device",
        "xbee:network",
        "xbee:network:schedules",
        "xbee:network:timeouts",
    ):
        if section not in config:
            config.add_section(section)

    buf = io.StringIO()
    config.write(buf)
    logger.debug("Configuration:\n{}".format(buf.getvalue().strip()))

    logger.debug(
        "Using port {port}@{baudrate}".format(port=port, baudrate=baudrate)
    )
    xbee_device = XBeeDevice(port, baudrate)
    xbee_device.set_sync_ops_timeout(
        config["xbee:device"].getfloat("sync_ops_timeout", 2)
    )
    xbee_central = XBeeMsgPackRPC(
        device=xbee_device,
        min_comm_interval=config["xbee:network"].getfloat(
            "min_comm_interval", 1
        ),
    )

    def start_client(client):
        logger.info(
            "Starting MQTT client {client._client_id} connecting to "
            "broker {client._host}:{client._port}...".format(client=client)
        )

        def on_connect(client, userdata, flags, rc):
            if rc:
                logger.error(
                    "Client {client._client_id} couldn't connect to "
                    "{client._host}:{client._port}: result code {rc}".format(
                        rc=rc, client=client
                    )
                )
            else:
                logger.info(
                    "Client {client._client_id} now connected to "
                    "{client._host}:{client._port}".format(client=client)
                )

        client.on_connect = on_connect
        client.loop_start()
        return client

    try:
        mqtt_clients = tuple(map(start_client, config.clients))
    except BrokerSectionError as e:
        raise click.ClickException(
            "Error setting up MQTT clients from "
            "configuration: {error}".format(error=e)
        )

    if not mqtt_clients:
        logger.warning(
            "No MQTT brokers defined. "
            "No data will be published to any broker. "
        )

    def close_mqtt_clients():
        for client in mqtt_clients:
            logger.debug(
                "disconnecting MQTT client {client._client_id}".format(
                    client=client
                )
            )
            client.disconnect()

    ctx.call_on_close(close_mqtt_clients)

    stations = defaultdict(lambda: defaultdict(dict))
    sensor_formats = config.sensor_formats

    @xbee_central.data_recieved_callback_pre
    def register_communication(xbee_message):
        addr = str(xbee_message.remote_device.get_64bit_addr())
        stations[addr]["last-contact"] = time.time()

    def get_interval(key, default):
        try:
            return (
                default
                if config["xbee:network:schedules"].getboolean(key, True)
                else False
            )
        except ValueError:
            return config["xbee:network:schedules"].getfloat(key, default)

    interval = get_interval("device_check", 5)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        def check_device_connection():
            # just check the communication
            xbee_central.device.get_power_level()

    interval = get_interval("broadcast_time", 67)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        @xbee_central.only_if_device_open
        def broadcast_time():
            logger.info("Broadcasting time")
            xbee_central.broadcast(
                Notification(method="time", params=[int(time.time())])
            )

    interval = get_interval("broadcast_pressure", 233)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        @xbee_central.only_if_device_open
        def broadcast_pressure():
            logger.warning(
                "TODO: Broadcasting pressure is not yet implemented"
            )
            # pressure_hPa = round(random.uniform(980, 1020), 1)
            # xbee_central.broadcast(
            #     Notification(method="pressure", params=[pressure_hPa, "hPa"])
            # )

    interval = get_interval("fill_information", 23)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        @xbee_central.only_if_device_open
        def fill_station_information():
            network_devices = xbee_central.device.get_network().get_devices()
            # loop over discovered and registered devices
            for addr in filter(
                lambda a: not all(
                    map(
                        lambda x: stations[a].get(x),
                        ("id", "sensors", "heights"),
                    )
                ),
                set(
                    itertools.chain(
                        map(
                            lambda d: str(d.get_64bit_addr()), network_devices
                        ),
                        stations,
                    )
                ),
            ):
                logger.info(
                    "Requesting format information for {addr}".format(
                        addr=addr
                    )
                )

                def callback(response, address):
                    if not isinstance(response, Response):
                        logger.error(
                            "Not a format response: {}".format(response)
                        )
                        return
                    if not isinstance(response.result, dict):
                        logger.error(
                            "Strange format: {}".format(response.result)
                        )
                        return
                    name = response.result.get("n")
                    if not name and not stations[address].get("id"):
                        name = address
                    sensors = response.result.get("s", tuple())
                    heights = response.result.get("h", tuple())
                    stations[address]["id"] = name
                    stations[address]["sensors"] = sensors
                    stations[address]["heights"] = heights
                    logger.debug(
                        "Updated station {id} ({addr}):\n{fmt}".format(
                            id=repr(name), addr=address, fmt=stations[address]
                        )
                    )

                request = xbee_central.client.request(
                    method="format",
                    callback=callback,
                    callback_kwargs={"address": addr},
                )
                remote_xbee = RemoteXBeeDevice(
                    xbee_central.device, XBee64BitAddress.from_hex_string(addr)
                )
                xbee_central.send(request, remote_xbee)

    interval = get_interval("forget_unresponsive", 45)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        def forget_unresponsive_xbees():
            unresponsive = tuple(
                filter(
                    lambda a: time.time()
                    - stations[a].get("last-contact", time.time())
                    > 60,
                    stations,
                )
            )
            for addr in unresponsive:
                logger.info(
                    "Forgetting XBee address {addr} because the "
                    "last successful contact was too long ago".format(
                        addr=addr
                    )
                )
                if config["xbee:network"].getboolean("reset_unresponsive"):
                    logger.debug("Resetting Station via XBee AT Command")
                    try:
                        reset_xbee = RemoteXBeeDevice(
                            xbee_device, XBee64BitAddress.from_hex_string(addr)
                        )
                        reset_xbee.set_io_configuration(
                            IOLine.DIO1_AD1, IOMode.DIGITAL_OUT_LOW
                        )
                        logger.info("Set the DIO1 Pin to LOW")
                    except XBeeException as e:
                        logger.error(
                            "Could NOT set the DIO1 Pin to LOW: "
                            "{error}".format(error=e)
                        )
                    logger.info(
                        "Waiting 2 seconds until setting DIO1 to Input"
                    )
                    time.sleep(2)
                    logger.info("Setting DIO1 to Input")
                    try:
                        reset_xbee = RemoteXBeeDevice(
                            xbee_device, XBee64BitAddress.from_hex_string(addr)
                        )
                        reset_xbee.set_io_configuration(
                            IOLine.DIO1_AD1, IOMode.DIGITAL_IN
                        )
                        logger.info("Set the DIO1 Pin to Input ")
                    except XBeeException as e:
                        logger.error(
                            "Could NOT set the DIO1 Pin to INPUT: "
                            "{error}".format(error=e)
                        )
                stations.pop(addr, None)
            if unresponsive:
                logger.debug(
                    "Stations:\n{}".format(
                        json.dumps(stations, indent=4, sort_keys=True)
                    )
                )

    interval = get_interval("discover_network", 311)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        @xbee_central.only_if_device_open
        def discover_network():
            network = xbee_central.device.get_network()
            network.clear()  # forget all previously found XBees
            found = set()

            def discover_callback(remote_xbee):
                addr = str(remote_xbee.get_64bit_addr())
                if addr not in found:
                    logger.debug("Discovered XBee {}".format(remote_xbee))
                    found.add(addr)

            def finished_callback(status):
                if status != NetworkDiscoveryStatus.SUCCESS:
                    logger.error(
                        "Network discovery finished. Status: {}".format(status)
                    )

            network.add_device_discovered_callback(discover_callback)
            network.add_discovery_process_finished_callback(finished_callback)
            logger.info("Discovering network...")
            network.start_discovery_process()
            time_before = time.time()
            while True:
                if not network.is_discovery_running():
                    logger.info(
                        "Network discovery done. {} devices discovered".format(
                            len(network.get_devices())
                        )
                    )
                    break
                if time.time() - time_before > 20:
                    logger.error("Device discovery takes to long. Aborting.")
                    network.stop_discovery_process()
                    break
                time.sleep(0.1)
            for client in mqtt_clients:
                station_name = getattr(
                    client, "_station_name", config.DEFAULT_CLIENT_STATION_NAME
                )
                stats = {
                    station_name: {
                        "xbee": {"network": {"devices": len(found)}}
                    }
                }
                for topic, message in map(
                    lambda x: ("/".join(x[0]), x[1]), utils.walk(stats)
                ):
                    msginfo = client.publish(topic, message, retain=True)
                    try:
                        if msginfo.is_published:
                            logger.debug(
                                "Client {client._client_id} on "
                                "{client._host}:{client._port} "
                                "successfully published {msg} "
                                "to {topic}".format(
                                    msg=message, topic=topic, client=client
                                )
                            )
                        else:
                            logger.debug(
                                "Client {client._client_id} on "
                                "{client._host}:{client._port} "
                                "queued {msg} for publishment "
                                "to {topic}".format(
                                    msg=message, topic=topic, client=client
                                )
                            )
                    except ValueError as e:
                        logger.debug(
                            "Client {client._client_id} on "
                            "{client._host}:{client._port} "
                            "could not queue {msg} for "
                            "publishment to {topic}: {error}".format(
                                msg=message,
                                topic=topic,
                                error=e,
                                client=client,
                            )
                        )

    def publish_measurement(measurement):
        stats = defaultdict(int)
        for client in mqtt_clients:
            stats["clients"] += 1
            topic_template = getattr(
                client,
                "_data_topic_template",
                config.DEFAULT_CLIENT_DATA_TOPIC_TEMPLATE,
            )
            try:
                topic = topic_template.format(**measurement)
            except BaseException as e:
                logger.error(
                    "Invalid topic template for client "
                    "{client._client_id} on "
                    "{client._host}:{client._port}: {etype}: "
                    "{error}".format(
                        client=client, etype=type(e).__name__, error=e
                    )
                )
                stats["errors"] += 1
                continue
            message_template = getattr(
                client,
                "_data_message_template",
                config.DEFAULT_CLIENT_DATA_MESSAGE_TEMPLATE,
            )
            try:
                message = message_template.format(**measurement)
            except BaseException as e:
                logger.error(
                    "Invalid message template for client "
                    "{client._client_id} on "
                    "{client._host}:{client._port}: {etype}: "
                    "{error}".format(
                        client=client, etype=type(e).__name__, error=e
                    )
                )
                stats["errors"] += 1
                continue
            msginfo = client.publish(topic, message)
            try:
                if msginfo.is_published:
                    logger.debug(
                        "Client {client._client_id} on "
                        "{client._host}:{client._port} "
                        "successfully published {msg} "
                        "to {topic}".format(
                            msg=message, topic=topic, client=client
                        )
                    )
                else:
                    logger.debug(
                        "Client {client._client_id} on "
                        "{client._host}:{client._port} "
                        "queued {msg} for publishment "
                        "to {topic}".format(
                            msg=value, topic=topic, client=client
                        )
                    )
                stats["publishments"] += 1
            except ValueError as e:
                logger.debug(
                    "Client {client._client_id} on "
                    "{client._host}:{client._port} "
                    "could not queue {msg} for "
                    "publishment to {topic}: {error}".format(
                        msg=value, topic=topic, error=e, client=client
                    )
                )
                stats["errors"] += 1
        return stats

    interval = get_interval("request_measurements", 37)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        @xbee_central.only_if_device_open
        def request_measurements():
            for addr, info in stations.items():
                if not (
                    info.get("id")
                    and info.get("heights")
                    and info.get("sensors")
                ):
                    logger.debug(
                        "Not enough info on {id} ({address}). "
                        "Skip requesting measurements.".format(
                            id=repr(info.get("id")), address=addr
                        )
                    )
                    continue
                logger.debug("Requesting measurements from {}".format(addr))

                recieved_heights = set()  # gets filled in the callback
                missing_heights = set(info["heights"])

                # loop until we recieved all heights from this station
                while True:
                    logger.debug(
                        "We need data from heights {} from station {}".format(
                            sorted(missing_heights), repr(info["id"])
                        )
                    )

                    def callback(response):
                        if not isinstance(response, Response):
                            logger.error(
                                "Not a measurement response: {}".format(
                                    response
                                )
                            )
                            return
                        logger.debug(
                            "Recieved measurements from {}: {}".format(
                                remote_xbee.get_64bit_addr(), response.result
                            )
                        )
                        stats = defaultdict(int)
                        for measurement in zip_measurements(
                            station_data=response.result,
                            station_sensors=info["sensors"],
                            sensor_heights=sorted(missing_heights),
                            sensor_formats=sensor_formats,
                        ):
                            recieved_heights.add(measurement["height"])
                            if measurement["value"] is None:
                                continue
                            measurement["station"] = info["id"]
                            logger.debug(
                                "Publishing measurements "
                                "from station {}: {}".format(
                                    repr(info["id"]), measurement
                                )
                            )
                            s = publish_measurement(measurement)
                            stats["publishments"] += s["publishments"]
                            stats["errors"] += s["errors"]
                            stats["clients"] = max(
                                stats["clients"], s["clients"]
                            )
                        stats["publishments"]
                        stats["errors"]
                        stats["clients"]
                        # TODO: ist nicht so schön, da es einen Error
                        # ausspuckt, falls von Anfang an keine Werte vorhanden
                        logger.info(
                            "Published a total of {publishments} messages "
                            "with {errors} errors "
                            "to a total of {clients} MQTT brokers".format(
                                **stats
                            )
                        )

                    request = xbee_central.client.request(
                        method="get",
                        params={"heights": sorted(missing_heights)},
                        callback=callback,
                    )
                    remote_xbee = RemoteXBeeDevice(
                        xbee_central.device,
                        XBee64BitAddress.from_hex_string(addr),
                    )
                    xbee_central.send(request, remote_xbee)
                    logger.debug(
                        "Waiting for measurement response "
                        "from station {}".format(repr(info["id"]))
                    )
                    if not xbee_central.client.wait_for_response(
                        request,
                        timeout=config["xbee:network:timeouts"].getfloat(
                            "measurement_response", 10
                        ),
                    ):
                        logger.error(
                            "Station {} didn't send "
                            "measurements within timeout".format(
                                repr(info["id"])
                            )
                        )
                        break
                    new_missing_heights = (
                        set(info["heights"]) - recieved_heights
                    )
                    if not new_missing_heights < missing_heights:
                        logger.error(
                            "Station {} didn't send new heights".format(
                                repr(info["id"])
                            )
                        )
                        break
                    if not new_missing_heights:
                        logger.debug(
                            "Recieved all {} heights {} for station {}".format(
                                len(info["heights"]),
                                info["heights"],
                                repr(info["id"]),
                            )
                        )
                        break
                    missing_heights = new_missing_heights

    interval = get_interval("request_status", 53)
    if interval:

        @xbee_central.scheduler.schedule(IntervalRepeater(interval=interval))
        @xbee_central.only_if_device_open
        def request_status():
            for addr, info in stations.items():
                logger.debug(
                    "requesting status from {address}".format(address=addr)
                )
                if not (info.get("id")):
                    logger.debug(
                        "Not enough info on {id} ({address}). "
                        "Skip requesting status.".format(
                            id=repr(info.get("id")), address=addr
                        )
                    )
                    continue

                logger.debug("Requesting status from {}".format(addr))

                def callback(response):

                    if not isinstance(response, Response):
                        logger.error(
                            "Not a status response: {}".format(response)
                        )
                        return
                    logger.debug(
                        "Recieved status from {}: {}".format(
                            remote_xbee.get_64bit_addr(), response.result
                        )
                    )
                    for component, status in response.result.items():
                        for client in mqtt_clients:
                            topic_template = getattr(
                                client,
                                "_status_topic_template",
                                config.DEFAULT_CLIENT_STATUS_TOPIC_TEMPLATE,
                            )
                            try:
                                topic = topic_template.format(
                                    component=component, station=info.get("id")
                                )
                            except BaseException as e:
                                logger.error(
                                    "Invalid status topic template for client "
                                    "{client._client_id} on "
                                    "{client._host}:{client._port}: {etype}: "
                                    "{error}".format(
                                        client=client,
                                        etype=type(e).__name__,
                                        error=e,
                                    )
                                )
                                continue
                            msginfo = client.publish(
                                topic, status, retain=True
                            )
                            try:
                                if msginfo.is_published:
                                    logger.debug(
                                        "Client {client._client_id} on "
                                        "{client._host}:{client._port} "
                                        "successfully published {msg} "
                                        "to {topic}".format(
                                            msg=status,
                                            topic=topic,
                                            client=client,
                                        )
                                    )
                                else:
                                    logger.debug(
                                        "Client {client._client_id} on "
                                        "{client._host}:{client._port} "
                                        "queued {msg} for publishment "
                                        "to {topic}".format(
                                            msg=status,
                                            topic=topic,
                                            client=client,
                                        )
                                    )
                            except ValueError as e:
                                logger.debug(
                                    "Client {client._client_id} on "
                                    "{client._host}:{client._port} "
                                    "could not queue {msg} for "
                                    "publishment to {topic}: {error}".format(
                                        msg=status,
                                        topic=topic,
                                        error=e,
                                        client=client,
                                    )
                                )
                    stats = defaultdict(int)
                    logger.debug(
                        "Publishing status "
                        "from station {}: {}".format(repr(info["id"]), status)
                    )

                request = xbee_central.client.request(
                    method="status",
                    # getting single hardware status response
                    # params={"RTC"},
                    # params={"Sd"},
                    # params={"Multiplexer"},
                    callback=callback,
                )
                remote_xbee = RemoteXBeeDevice(
                    xbee_central.device, XBee64BitAddress.from_hex_string(addr)
                )
                xbee_central.send(request, remote_xbee)
                logger.debug(
                    "Waiting for status response "
                    "from station {}".format(repr(info["id"]))
                )
                if not xbee_central.client.wait_for_response(
                    request,
                    timeout=config["xbee:network:timeouts"].getfloat(
                        "status_response", 10
                    ),
                ):
                    logger.error(
                        "Station {} didn't send "
                        "status within timeout".format(repr(info["id"]))
                    )

    @xbee_central.dispatcher.method
    def register(request, xbee_message, *args, **kwargs):
        addr = str(xbee_message.remote_device.get_64bit_addr())
        try:
            station_id = request.params["id"]
        except (TypeError, KeyError, IndexError):
            logger.debug(
                "XBee {addr} didn't register with an 'id'. "
                "Using its address as identifier instead.".format(addr=addr)
            )
            station_id = addr
        logger.info(
            "Associating XBee address {addr} with station id {id} "
            "and forgetting previous format info".format(
                addr=addr, id=repr(station_id)
            )
        )
        stations[addr] = {"id": station_id}
        return Response(id=request.id, result="registered")

    @xbee_central.dispatcher.method
    def status(request, xbee_message, *args, **kwargs):
        logger.debug("Got status message! {request}".format(request=request))
        addr = str(xbee_message.remote_device.get_64bit_addr())
        info = stations.get(addr, {})
        for component, status in request.params.items():
            logger.debug(status)
            for client in mqtt_clients:
                if isinstance(status, dict):
                    topic_template = getattr(
                        client,
                        "_status_sensors_topic_template",
                        config.DEFAULT_CLIENT_STATUS_SENSORS_TOPIC_TEMPLATE,
                    )
                    message = status["works"]
                else:
                    topic_template = getattr(
                        client,
                        "_status_sensors_topic_template",
                        config.DEFAULT_CLIENT_STATUS_TOPIC_TEMPLATE,
                    )
                    message = status
                try:
                    if isinstance(status, dict):
                        topic = topic_template.format(
                            component=component,
                            station=info.get("id", "?"),
                            **status
                        )
                    else:
                        topic = topic_template.format(
                            component=component, station=info.get("id", "?")
                        )
                except BaseException as e:
                    logger.error(
                        "Invalid status topic template for client "
                        "{client._client_id} on "
                        "{client._host}:{client._port}: {etype}: "
                        "{error}".format(
                            client=client, etype=type(e).__name__, error=e
                        )
                    )
                    continue
                msginfo = client.publish(topic, message, retain=True)
                try:
                    if msginfo.is_published:
                        logger.debug(
                            "Client {client._client_id} on "
                            "{client._host}:{client._port} "
                            "successfully published {msg} "
                            "to {topic}".format(
                                msg=status, topic=topic, client=client
                            )
                        )
                    else:
                        logger.debug(
                            "Client {client._client_id} on "
                            "{client._host}:{client._port} "
                            "queued {msg} for publishment "
                            "to {topic}".format(
                                msg=status, topic=topic, client=client
                            )
                        )
                except ValueError as e:
                    logger.debug(
                        "Client {client._client_id} on "
                        "{client._host}:{client._port} "
                        "could not queue {msg} for "
                        "publishment to {topic}: {error}".format(
                            msg=status, topic=topic, error=e, client=client
                        )
                    )
        stats = defaultdict(int)
        logger.debug(
            "Publishing status "
            "from station {}: {}".format(repr(info.get("id", "?")), status)
        )

    try:
        logger.debug("opening XBee device")
        xbee_central.run()
    except KeyboardInterrupt:
        logger.info("stopping (might take a couple of seconds)...")
    finally:
        try:
            xbee_central.stop()
        except BaseException as e:
            logger.debug(
                "{etype} during stopping of XBee central: {error}".format(
                    etype=type(e).__name__, error=e
                )
            )
